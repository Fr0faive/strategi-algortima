#include <iostream>
#include <vector>
#include <cmath>
#include <climits>

bool isValidCombination(const std::vector<int>& combination, int totalDistance, int restDistance) {
    int distance = 0;
    for (int i = 0; i < combination.size(); i++) {
        if (combination[i] - distance > restDistance) {
            return false;
        }
        distance = combination[i];
    }
    if (totalDistance - distance > restDistance) {
        return false;
    }
    return true;
}

int main() {
    std::vector<int> warung = {10, 25, 30, 40, 50, 75, 80, 110, 130};
    int minStops = INT_MAX;
    std::vector<int> bestCombination;

    for (int i = 0; i < (1 << warung.size()); i++) {
        std::vector<int> combination;
        for (int j = 0; j < warung.size(); j++) {
            if ((i & (1 << j)) != 0) {
                combination.push_back(warung[j]);
            }
        }

        if (isValidCombination(combination, 140, 30)) {
            int numStops = combination.size();
            if (numStops < minStops) {
                minStops = numStops;
                bestCombination = combination;
            }
        }
    }

    std::cout << "Titik-titik warung Tono akan berhenti: ";
    for (int i = 0; i < bestCombination.size(); i++) {
        std::cout << bestCombination[i] << " ";
    }
    std::cout << std::endl;

    return 0;
}
