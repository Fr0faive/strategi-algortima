# Tugas Strategi Algoritma 2
Tugas ini dibuat menggunakan bahasa python

```
import time

k = 1
jumlah = 0
n = 10

start_time = time.time_ns() / (10**6)

while k <= n:
    jumlah += k
    k += 1

r = jumlah / n
print("Rata-rata : ", r)

end_time = time.time_ns()
elapsed_time = (end_time - start_time) / 1000

print("Waktu yang diperlukan:", elapsed_time, "mikrodetik")


```
